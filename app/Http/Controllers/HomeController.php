<?php

namespace App\Http\Controllers;
use App\Models\Question;
use Illuminate\Http\Request;;
use Symfony\Component\HttpFoundation\Cookie;
use App\Models\Answer;

class HomeController extends Controller
{

    public function get()
    {
        return view('home');
    }

    public function quiz(Request $request)
    {
        $score = $request->input('score');
        if(empty($score)) $score = 0;
        $question = Question::orderBy('order','ASC')->paginate(4);
        $question = $question->toarray();
        $question['score'] = $score;
//        dd($question);
        return view('question',$question);
    }

    public function summary(Request $request)
    {
        $score = $request->input('score');
        if($score > 62) $score = 62;
        $answer = Answer::where('score','>=',$score)->orderBy('score','ASC')->first();
        return view('answer',$answer);
    }

}
