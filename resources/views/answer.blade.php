<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>


        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
{{--        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">--}}
{{--        <link href='https://fonts.googleapis.com/css?family=Kanit:400,300&subset=thai,latin' rel='stylesheet' type='text/css'>--}}

        <!-- Styles -->
        <style>

        @font-face {
            font-family: 'Kanit';
            font-style: normal;
            font-weight: 700;
            src: local('Kanit Regular'), local('Kanit-Regular'), url({!! url('/kanit/Kanit-Regular.ttf') !!}) format('truetype');
        }
            html, body {
                color: white;
                font-family: 'Kanit', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                overflow: hidden;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            #backgroundImage {
                background: url({!! url('/images/bg.png') !!}) no-repeat 0 100%;
                background-size: 100% 100%;
                padding-top: 25%;
                padding-left: 3%;
                padding-right: 3%;

            }

            .answer .header{
                font-size: 4rem;
            }

            .answer .header span{
                display: block;
                width: 75%;
            }

            .answer .excerpt {
                font-size: 2.3rem;
            }

            .answer .excerpt span{
                display: block;
                width: 90%;
            }

            .answer .content{
                border-radius: 50px 50px 50px 50px;
                color: #4C2983;
                margin-top: 30px;
                background-color: white;
                font-weight: bold;
                text-align: left;
                font-size: 2.5rem;
                padding: 40px;
            }



        </style>
    </head>
    <body id="backgroundImage">
        <div class="answer">
            <div class="header">
                <span>ความเครียดของคุณอยู่ในระดับ {!! $score_title; !!}</span>
            </div>
            <div class="excerpt">
                <span>{!! $excerpt; !!}</span>
            </div>
            <div class="content">
                <span>{!! $content; !!}</span>
            </div>
        </div>
    </body>
<script src="{!! url('jquery-3.5.1.slim.min.js') !!}" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous">
</script>
<script>
    $(document).on('click','body',function() {

            if(confirm("คุณต้องการจะทดสอบใหม่อีกครั้งไหม?")){
                $(location).attr('href','{!! url('/') !!}');
            }
            else{
                return false;
            }
    });
</script>
</html>
