<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>


        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
{{--        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">--}}
{{--        <link href='https://fonts.googleapis.com/css?family=Kanit:400,300&subset=thai,latin' rel='stylesheet' type='text/css'>--}}
        <!-- Styles -->
        <style>

        @font-face {
            font-family: 'Kanit';
            font-style: normal;
            font-weight: 700;
            src: local('Kanit Regular'), local('Kanit-Regular'), url({!! url('/kanit/Kanit-Regular.ttf') !!}) format('truetype');
        }
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Kanit', sans-serif;
                font-weight: 200;
                height: 100%;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                /*text-align: center;*/
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            #backgroundImage {
                background: url({!! url('/images/home.jpg') !!}) no-repeat 0 100%;
                background-size: 100% 100%;
            }
            .container {
                color: white;
            }
            #button-quiz {
                width: 100%;
                text-align: center;
            }

            #session1 {
                padding-top: 25%;
                padding-left: 2%;
                padding-bottom: 1%;
            }
            #session1 .header{
                font-size: 3.1rem;
                font-weight: bold;
                margin-left: 20px;
                padding-left: 10px;
                height: 100px;
                background-image: url({!! url('/images/pink.png') !!});
                background-repeat: no-repeat;
                background-size: 75%;

            }
            #session1 .content, #session2 .content {
                font-size: 2rem;
                padding-top: 10px;
                padding-bottom: 10px;
            }

            #session1 .content span{
                display: block;
                width: 75%;
                padding-left: 20px;
            }

            #session2 .content table{
                width: 100%;
                padding-left: 20px;
            }

            #session2 .content td {
                font-size: 1.5rem;
                padding-right: 1.5%;
            }

            #session2 {
                padding-top: 5%;
                padding-left: 2%;
                padding-bottom: 1%;
            }
            #session2 .header{
                font-size: 1.8rem;
                font-weight: bold;
                margin-left: 20px;
                padding-left: 10px;
                height: 100px;
                background-image: url({!! url('/images/yellow.png') !!});
                background-repeat: no-repeat;
                background-size: 95%;
            }
            #session2 .header span{
                padding-top: 3%;
                display: table-cell;
                vertical-align: middle;
            }

            {{--#session3 .header{--}}
            {{--    font-size: 1.8rem;--}}
            {{--    font-weight: bold;--}}
            {{--    margin-left: 20px;--}}
            {{--    padding-left: 10px;--}}
            {{--    height: 100px;--}}
            {{--    background-image: url({!! url('/images/blue.png') !!});--}}
            {{--    background-repeat: no-repeat;--}}
            {{--    background-size: 95%;--}}
            {{--}--}}

            #session3 {
                margin-top: 10%;
                padding-left: 2%;
            }
            #session3 .header{
                font-size: 2.5rem;
                font-weight: bold;
            }
            #session3 .header span{
                display: inline-block;
                text-align: right;
                width: 35%;
            }

        </style>
    </head>
    <body id="backgroundImage">

        <div class="container">
            <div id="session1" >
                <div class="header">
                    <strong>มาวัดความเครียดกันเถอะ</strong>
                </div>
                <div class="content">
                    <span>
                        ในระยะ 6 เดือนที่ผ่านมามีเหตุการณ์ในข้อใด
                        เกิดขึ้นกับตัวคุณบ้างและคุณมีความรู้สึกอย่างไร
                        ต่อเหตุการณ์นั้น
                    </span>
                </div>
            </div>
            <div id="session2" >
                <div class="header">
                    <span>ในแต่ละคำถามให้เลือกระดับความเครียดตารมเกณฑ์ดังนี้</span>
                </div>
                <div class="content">
                    <table>
                        <tr>
                            <td>ระดับความเครียด</td>
                            <td>1</td>
                            <td>หมายถึง</td>
                            <td>ไม่รู้สึกเครียด</td>
                        </tr>
                        <tr>
                            <td>ระดับความเครียด</td>
                            <td>2</td>
                            <td>หมายถึง</td>
                            <td>รู้สึกเครียดเล็กน้อย</td>
                        </tr>
                        <tr>
                            <td>ระดับความเครียด</td>
                            <td>3</td>
                            <td>หมายถึง</td>
                            <td>รู้สึกเครียดปานกลาง</td>
                        </tr>
                        <tr>
                            <td>ระดับความเครียด</td>
                            <td>4</td>
                            <td>หมายถึง</td>
                            <td>รู้สึกเครียดมาก</td>
                        </tr>
                        <tr>
                            <td>ระดับความเครียด</td>
                            <td>5</td>
                            <td>หมายถึง</td>
                            <td>รู้สึกเครียดมากที่สุด</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="session3" >
                <div class="header">
                    <span>หากพร้อมแล้วมาเริ่มกันเลย</span>
                    <a href="/quiz">
                        <img src="{!! url('/images/blue.png') !!}" style="width: 40%" >
                    </a>
                </div>
            </div>
        </div>


    </body>
{{--        <script language="javascript">--}}
{{--        function autoResizeDiv()--}}
{{--        {--}}
{{--            document.getElementById('homeImage').style.height = window.innerHeight +'px';--}}
{{--        }--}}
{{--        window.onresize = autoResizeDiv;--}}
{{--        autoResizeDiv();--}}
{{--    </script>--}}

</html>
