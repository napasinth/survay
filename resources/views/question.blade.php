<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>


        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
{{--        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">--}}
{{--        <link href='https://fonts.googleapis.com/css?family=Kanit:400,300&subset=thai,latin' rel='stylesheet' type='text/css'>--}}


        <!-- Styles -->
        <style>

        @font-face {
            font-family: 'Kanit';
            font-style: normal;
            font-weight: 700;
            src: local('Kanit Regular'), local('Kanit-Regular'), url({!! url('/kanit/Kanit-Regular.ttf') !!}) format('truetype');
        }
            html, body {
                color: white;
                font-family: 'Kanit', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                overflow:hidden;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            #backgroundImage {
                background: url({!! url('/images/bg.png') !!}) no-repeat 0 100%;
                background-size: 100% 100%;
                padding-top: 30%;
                padding-left: 3%;

            }
            .question .label{
                font-size: 2rem;
            }
            .question .header {
                font-size: 2rem;
                /*margin-left: 20px;*/
                padding-left: 10px;
                height: 105px;
                background-image: url({!! url('/images/1.png') !!});
                background-repeat: no-repeat;
                background-size: 90%;
                display: table;
                width: 100%;
            }

            .question .bg1{
                background-image: url({!! url('/images/1.png') !!});
            }

            .question .bg2{
                background-image: url({!! url('/images/2.png') !!});
            }

            .question .bg3{
                background-image: url({!! url('/images/3.png') !!});
            }

            .question .bg4{
                background-image: url({!! url('/images/4.png') !!});
            }

            .question .header span{
                display: table-cell;
                padding-top: 3%;
            }

            .choices .choice {
                font-size: 3rem;
                margin-right: 20px;
                /*padding-left: 10px;*/
                height: 110px;
                background-image: url({!! url('/images/circle.png') !!});
                background-repeat: no-repeat;
                background-size: 100%;
                width: 103px;
                height: 103px;
                display: table-cell;
                color: black;
                text-align: center;
                font-weight: bold;
                vertical-align: middle;
                cursor: pointer;
            }

            .choices .choice-selected {
                background-image: url({!! url('/images/circle-selected.png') !!});
            }

        </style>
    </head>
    <body id="backgroundImage">
        @foreach($data as $key => $value)
        <div class="question">
            <div class="label"><span>คำถามที่ {!! $value['order'] !!}</span></div>
            <div class="header bg{!! ($key+1) !!}">
                <span>{!! $value['title'] !!}</span>
            </div>
            <div class="choices">
                <span class="choice" datatype="{!! $value['order'] !!}" >1</span>
                <span class="choice" datatype="{!! $value['order'] !!}" >2</span>
                <span class="choice" datatype="{!! $value['order'] !!}" >3</span>
                <span class="choice" datatype="{!! $value['order'] !!}" >4</span>
                <span class="choice" datatype="{!! $value['order'] !!}" >5</span>
            </div>
        </div>
        @endforeach
    </body>
<script src="{!! url('jquery-3.5.1.slim.min.js') !!}" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous">
</script>
<script>
    var answers = [];
    var postScore = {!! $score; !!};
    var score;
    var sumScore;
    var nextPageUrl = '{!! $next_page_url !!}';
    $(document).on('click','.choice',function(){
        var questionNo = this.getAttribute('datatype');
        var choice = this.innerHTML;

        $(this).parent().find('span').removeClass("choice-selected");
        // alert($(this).parent().attr('class'));
        $(this).removeClass('choice').addClass("choice-selected choice");
        // saveQuiz(1,choice);
        answers[questionNo] = choice;
        // alert(countNonEmpty(answers))
        // alert(JSON.stringify(answers));
        if(countNonEmpty(answers) == 4)
        {

           sumScore = answers.reduce(function(a, b){
                a = parseInt(a);
                b = parseInt(b);
                return a+b;
           });
           postScore = parseInt(postScore);
           sumScore = parseInt(sumScore);
           score = postScore+sumScore;

           if(nextPageUrl == '')
           {
               nextPageUrl = '{!! url('/quiz/summary') !!}';
               $(location).attr('href',nextPageUrl+'?score='+score);
           } else {
               $(location).attr('href',nextPageUrl+'&score='+score);
           }


        }

    });

    function countNonEmpty(array) {
        return array.filter(Boolean).length;
    }

    function sumAction(a,b){


        a = parseInt(a);
        b = parseInt(b);
        if (isNaN(a) || isNaN(b)) alert("wrong input")
        else return a + b;
    }
// $(location).attr('href','http://www.example.com')

</script>
</html>
